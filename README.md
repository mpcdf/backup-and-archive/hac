# has and hac - hpssadm wrapper

'hac and has' is developed by MAX PLANCK COMPUTING AND DATA FACILITY and published under GNU GENERAL PUBLIC LICENSE Version 3.
For questions and suggestions please contact Elena Summer elena.summer (at) mpcdf.mpg.de

'hac and has' is a client-server application. 
'has' is a server part. It takes care of starting and running hpssadm and waits for client commands. 
'hac' is a client part. It translates simple 'hac' commands to complicated hpssadm commands and sends them to 'has'.
'hac.dict' is used by 'hac' to translate 'hac' commands into hpssadm commands.

MAX PLANCK COMPUTING AND DATA FACILITY has no responsibility for data damage or data loss when using this software.
Try it first on a test system to be sure it behaves appropriately.

## Installation ##

You can use 'hac and has' code either from branch 'develop' or branch 'main' or from tags. Branch 'develop' contains latest changes. It is a newest version. Branch 'main' contains latest release. Tags point to different, also previous, releases.

'has' and 'hac' are installed and used only on the machine where HPSS is running - HPSS core machine. 'has' accepts connections only from localhost.

'has' should run under the Linux user which can run 'hpssadm'.

'hac' can run under any Linux user registered on the HPSS core machine. 'hac' executable should be in this user's PATH environmental variable. 'hac.log' should be accessible for this user to write. This means that any user who can login to HPSS core machine, can run 'hac' commands and change anything in HPSS. If you do not want any user to run 'hac', you can make 'hac' executable not accessible for this user. Or you can implement a more fine grained user management in 'has' and 'hac'.

To install 'has' and 'hac', copy 'hac', 'has' and 'hac.dict' in a directory which should be in user's PATH, for example /usr/local/bin/.

Create a 'has' systemctl service. Start 'has' with 'systemctl start has'. Then use 'hac' commands. Type "hac help" for details. 'hac' has a serachable help. You can type for example 'hac help create tape' for commands to create tapes.

systemctl service is used by 'hac' to restart 'has' if it is not running. Therefore, you should either create the 'has' systemctl service or change 'hac' code to not use it. If you don't use a 'has' systemctl service, you can start 'has' by just typing "has" in terminal. Please be aware that if running 'hac' as a non-root user, restart a systemctl service will not work. In this case you need to implement a different way of restarting 'has'.

'hac.log' and 'has.log' log files as well as 'hac.dict' are located in the same directory where the executables - 'hac' and 'has' - reside . Please change the code to use different directories.

hpssadm is called by 'has' via "/opt/hpss/bin/hpssadm.pl -b -U <USER> -a <KEYTAB> -j /usr/bin 2>&1". Change it in 'has' code if you call it differently. Place user name and keytab file location. Correct the java location if it differs from "/usr/bin". We for example call 'hpssadm' with 'hpssssm' user. All users who can login on the HPSS core machine can run 'hac' commands. These commands will be sent to one 'hpssadm' instance running under one user. You can implement a more fine grained user management.

In 'hac', 'has', and 'hac.dict' code, look for '# ATTENTION:'. Check and change these parts of code if necessary.

## Prerequisites ##

- 'hac' works with HPSS version 7.5.3 and 9.2.0. It may work with other HPSS versions but we have not tested it. 
- perl
- hpssadm

## Implementing new 'hac' commands ##
Add new commands to 'hac.dict' and add unit tests for each new 'hac' command you implemented to 'hac_test.sh'.

## License ## 
'hac and has' is published under GNU GENERAL PUBLIC LICENSE Version 3.

## Contributing ##
If you wish to contribute to this project, please:
- crete your own branch and perform your changes there;
- add unit tests for each new 'hac' command you implemented to 'hac_test.sh'.
- when you are ready create a merge request in Gitlab with 'develop' as a target branch - here are instructions: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html .

Please be aware that your changes will be published under GNU GENERAL PUBLIC LICENSE Version 3.

