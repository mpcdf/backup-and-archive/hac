#!/bin/bash

#
# This file is part of has and hac - hpssadm wrapper
#
# Copyright (C) 2022 MPCDF
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

passed=1

function check(){
    if [ "$1" != "$2" ]; then
        echo "-----Test failed: $1 != $2"
        passed=$(($passed * 0))
    fi 
}

echo '-- configuration info commands'

echo '----positive:'

echo './hac conf info tsc 11 -t'
output=$(./hac conf info tsc 11 -t 2>&1)
expected="Going to send hpssadm_command = configuration info -type \"Tape Storage Class Configuration\" -id 11"
echo $output
check "$output" "$expected"
echo

echo '--decices and drives commands'
echo '---device list commands'

echo '----positive:'

echo './hac list devall -t'
output=$(./hac list devall -t 2>&1)
expected="Going to send hpssadm_command = device list -all"
echo $output
check "$output" "$expected"
echo

echo './hac list devsick -t'
output=$(./hac list devsick -t 2>&1)
expected="Going to send hpssadm_command = device list -sick"
echo $output
check "$output" "$expected"
echo

echo './hac list dev 12 -t'
output=$(./hac list dev 12 -t 2>&1)
expected="Going to send hpssadm_command = device list -id 12"
echo $output
check "$output" "$expected"
echo

echo './hac list dev 123 -'
output=$(./hac list dev 123 -t 2>&1)
expected="Going to send hpssadm_command = device list -id 123"
echo $output
check "$output" "$expected"
echo

echo './hac list dev 30-34,37,60-62 -t'
output=$(./hac list dev 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device list -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo

echo '--------negative'

echo './hac list dev -t'
output=$(./hac list dev -t 2>&1)
expected="Incorrect command: \"list dev -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo './hac list dev 1 -t'
output=$(./hac list dev 1 -t 2>&1)
expected="Incorrect command: \"list dev 1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac list dev 1234 -t'
output=$(./hac list dev 1234 -t 2>&1)
expected="Incorrect command: \"list dev 1234 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac list dev -1 -t'
output=$(./hac list dev -1 -t 2>&1)
expected="Incorrect command: \"list dev -1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac list dev la -t'
output=$(./hac list dev la -t 2>&1)
expected="Incorrect command: \"list dev la -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---device info commands'
echo '----positive'

echo './hac info drive 12 -t'
output=$(./hac info drive 12 -t 2>&1)
expected="Going to send hpssadm_command = device info -drive -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac info drive 123 -t'
output=$(./hac info drive 123 -t 2>&1)
expected="Going to send hpssadm_command = device info -drive -id 123"
echo $output
check "$output" "$expected"
echo

echo './hac info drive 30-34,37,60-62 -t'
output=$(./hac info drive 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device info -drive -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo

echo '--------negative'

echo './hac info drive -t'
output=$(./hac info drive -t 2>&1)
expected="Incorrect command: \"info drive -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo './hac info drive 1 -t'
output=$(./hac info drive 1 -t 2>&1)
expected="Incorrect command: \"info drive 1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo './hac info drive 1234 -t'
output=$(./hac info drive 1234 -t 2>&1)
expected="Incorrect command: \"info drive 1234 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo './hac info drive -1 -t'
output=$(./hac info drive -1 -t 2>&1)
expected="Incorrect command: \"info drive -1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo './hac info drive la -t'
output=$(./hac info drive la -t 2>&1)
expected="Incorrect command: \"info drive la -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac info dev 12 -t'
output=$(./hac info dev 12 -t 2>&1)
expected="Going to send hpssadm_command = device info -device -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac info dev 123 -t'
output=$(./hac info dev 123 -t 2>&1)
expected="Going to send hpssadm_command = device info -device -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac info dev 30-34,37,60-62 -t'
output=$(./hac info dev 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device info -device -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo

echo '--------negative'

echo './hac info dev -t'
output=$(./hac info dev -t 2>&1)
expected="Incorrect command: \"info dev -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info dev 1 -t'
output=$(./hac info dev 1 -t 2>&1)
expected="Incorrect command: \"info dev 1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info dev 1234 -t'
output=$(./hac info dev 1234 -t 2>&1)
expected="Incorrect command: \"info dev 1234 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info dev -1 -t'
output=$(./hac info dev -1 -t 2>&1)
expected="Incorrect command: \"info dev -1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info dev la -'
output=$(./hac info dev la -t 2>&1)
expected="Incorrect command: \"info dev la -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info dev 12 lalala -t'
output=$(./hac info dev 12 lalala -t 2>&1)
expected="Incorrect command: \"info dev 12 lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info dev lalala 12 -t'
output=$(./hac info dev lalala 12 -t 2>&1)
expected="Incorrect command: \"info dev lalala 12 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo '---device update commands'
echo '----positive'

echo './hac update dev 12 readon -t'
output=$(./hac update dev 12 readon -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Flags\" -subfield \"Read Enabled\" on"
echo $output
check "$output" "$expected"
echo


echo './hac update dev 122 readon -t'
output=$(./hac update dev 122 readon -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Flags\" -subfield \"Read Enabled\" on"
echo $output
check "$output" "$expected"
echo


echo '----negative'

echo './hac update dev 12 -t '
output=$(./hac update dev 12 -t 2>&1)
expected="Incorrect command: \"update dev 12 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 12 readonlalala -t'
output=$(./hac update dev 12 readonlalala -t 2>&1)
expected="Incorrect command: \"update dev 12 readonlalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 12 readon lalala -t'
output=$(./hac update dev 12 readon lalala -t 2>&1)
expected="Incorrect command: \"update dev 12 readon lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 1 readon -t'
output=$(./hac update dev 1 readon -t 2>&1)
expected="Incorrect command: \"update dev 1 readon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 1234 readon -t'
output=$(./hac update dev 1234 readon -t 2>&1)
expected="Incorrect command: \"update dev 1234 readon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev -1 readon -t'
output=$(./hac update dev -1 readon -t 2>&1)
expected="Incorrect command: \"update dev -1 readon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev la readon -t'
output=$(./hac update dev la readon -t 2>&1)
expected="Incorrect command: \"update dev la readon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac update dev 12 readoff -t'
output=$(./hac update dev 12 readoff -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Flags\" -subfield \"Read Enabled\" off"
echo $output
check "$output" "$expected"
echo


echo './hac update dev 122 readoff -t'
output=$(./hac update dev 122 readoff -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Flags\" -subfield \"Read Enabled\" off"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac update dev 12 -t'
output=$(./hac update dev 12 -t 2>&1)
expected="Incorrect command: \"update dev 12 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 12 readofflalala -t'
output=$(./hac update dev 12 readofflalala -t 2>&1)
expected="Incorrect command: \"update dev 12 readofflalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 12 readoff lalala -t'
output=$(./hac update dev 12 readoff lalala -t 2>&1)
expected="Incorrect command: \"update dev 12 readoff lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 1 readoff -t'
output=$(./hac update dev 1 readoff -t 2>&1)
expected="Incorrect command: \"update dev 1 readoff -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 1234 readoff -t'
output=$(./hac update dev 1234 readoff -t 2>&1)
expected="Incorrect command: \"update dev 1234 readoff -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev -1 readoff -t'
output=$(./hac update dev -1 readoff -t 2>&1)
expected="Incorrect command: \"update dev -1 readoff -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev la readoff -t'
output=$(./hac update dev la readoff -t 2>&1)
expected="Incorrect command: \"update dev la readoff -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac update dev 12 writeon -t'
output=$(./hac update dev 12 writeon -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Flags\" -subfield \"Write Enabled\" on"
echo $output
check "$output" "$expected"
echo


echo './hac update dev 122 writeon -t'
output=$(./hac update dev 122 writeon -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Flags\" -subfield \"Write Enabled\" on"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac update dev 12 -t'
output=$(./hac update dev 12 -t 2>&1)
expected="Incorrect command: \"update dev 12 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 12 writeonlalala -t'
output=$(./hac update dev 12 writeonlalala -t 2>&1)
expected="Incorrect command: \"update dev 12 writeonlalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 12 writeon lalala -t'
output=$(./hac update dev 12 writeon lalala -t 2>&1)
expected="Incorrect command: \"update dev 12 writeon lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 1 writeon -t'
output=$(./hac update dev 1 writeon -t 2>&1)
expected="Incorrect command: \"update dev 1 writeon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev 1234 writeon -t'
output=$(./hac update dev 1234 writeon -t 2>&1)
expected="Incorrect command: \"update dev 1234 writeon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev -1 writeon -t'
output=$(./hac update dev -1 writeon -t 2>&1)
expected="Incorrect command: \"update dev -1 writeon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update dev la writeon -t'
output=$(./hac update dev la writeon -t 2>&1)
expected="Incorrect command: \"update dev la writeon -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac update dev 12 writeoff -t'
output=$(./hac update dev 12 writeoff -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Flags\" -subfield \"Write Enabled\" off"
echo $output
check "$output" "$expected"
echo


echo './hac update dev 122 writeoff -t'
output=$(./hac update dev 122 writeoff -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Flags\" -subfield \"Write Enabled\" off"
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac reset dev 12 err -t'
output=$(./hac reset dev 12 err -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Record\" -subfield \"Number Of Errors\""
echo $output
check "$output" "$expected"
echo


echo './hac reset dev 122 err -t'
output=$(./hac reset dev 122 err -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Record\" -subfield \"Number Of Errors\""
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac reset dev 12 br -t'
output=$(./hac reset dev 12 br -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Record\" -subfield \"Bytes Read\""
echo $output
check "$output" "$expected"
echo

echo './hac reset dev 122 br -t'
output=$(./hac reset dev 122 br -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Record\" -subfield \"Bytes Read\""
echo $output
check "$output" "$expected"
echo

echo '----positive'

echo './hac reset dev 12 bw -t'
output=$(./hac reset dev 12 bw -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Record\" -subfield \"Bytes Written\""
echo $output
check "$output" "$expected"
echo


echo './hac reset dev 122 bw -t'
output=$(./hac reset dev 122 bw -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 122 -field \"Device Record\" -subfield \"Bytes Written\""
echo $output
check "$output" "$expected"
echo


echo '---drive update commands'
echo '----positive'

echo './hac reset drive 12 mnt -t'
output=$(./hac reset drive 12 mnt -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"PVL Drive Information\" -id 12 -field \"Mounts Since Last Maintenance\" 0"
echo $output
check "$output" "$expected"
echo


echo './hac reset drive 123 mnt -t'
output=$(./hac reset drive 123 mnt -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"PVL Drive Information\" -id 123 -field \"Mounts Since Last Maintenance\" 0"
echo $output
check "$output" "$expected"
echo

echo '---device lock commands'
echo '----positive'

echo './hac lock dev 12 -t'
output=$(./hac lock dev 12 -t 2>&1)
expected="Going to send hpssadm_command = device lock -device -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac unlock dev 12 -t'
output=$(./hac unlock dev 12 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -device -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac lock dev 123 -t'
output=$(./hac lock dev 123 -t 2>&1)
expected="Going to send hpssadm_command = device lock -device -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac unlock dev 123 -t'
output=$(./hac unlock dev 123 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -device -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac lock dev 30-34,37,60-62 -t'
output=$(./hac lock dev 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device lock -device -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo


echo './hac unlock dev 30-34,37,60-62 -t'
output=$(./hac unlock dev 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -device -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac lock dev -t'
output=$(./hac lock dev -t 2>&1)
expected="Incorrect command: \"lock dev -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac unlock dev 12 lalala -t'
output=$(./hac unlock dev 12 lalala -t 2>&1)
expected="Incorrect command: \"unlock dev 12 lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac lock dev 1234 -t'
output=$(./hac lock dev 1234 -t 2>&1)
expected="Incorrect command: \"lock dev 1234 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac unlock dev -1 -t'
output=$(./hac unlock dev -1 -t 2>&1)
expected="Incorrect command: \"unlock dev -1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac lock dev la -t'
output=$(./hac lock dev la -t 2>&1)
expected="Incorrect command: \"lock dev la -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---drive lock commands'
echo '----positive'

echo './hac lock drive 12 -t'
output=$(./hac lock drive 12 -t 2>&1)
expected="Going to send hpssadm_command = device lock -drive -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive 12 -t'
output=$(./hac unlock drive 12 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -drive -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac lock drive 123 -t'
output=$(./hac lock drive 123 -t 2>&1)
expected="Going to send hpssadm_command = device lock -drive -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive 123 -t'
output=$(./hac unlock drive 123 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -drive -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac lock drive 30-34,37,60-62 -t'
output=$(./hac lock drive 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device lock -drive -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive 30-34,37,60-62 -t'
output=$(./hac unlock drive 30-34,37,60-62 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -drive -id 30-34,37,60-62"
echo $output
check "$output" "$expected"
echo


echo './hac lock drive D00120 -t'
output=$(./hac lock drive D00120 -t 2>&1)
expected="Going to send hpssadm_command = device lock -drive -id 120"
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive D00120 -t'
output=$(./hac unlock drive D00120 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -drive -id 120"
echo $output
check "$output" "$expected"
echo


echo './hac lock drive D00123 -t'
output=$(./hac lock drive D00123 -t 2>&1)
expected="Going to send hpssadm_command = device lock -drive -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive D00123 -t'
output=$(./hac unlock drive D00123 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -drive -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac lock drive D00300-D00304,D00371,D00600-D00602 -t'
output=$(./hac lock drive D00300-D00304,D00371,D00600-D00602 -t 2>&1)
expected="Going to send hpssadm_command = device lock -drive -id 300,301,302,303,304,371,600,601,602"
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive D00300-D00304,D00371,D00600-D00602 -t'
output=$(./hac unlock drive D00300-D00304,D00371,D00600-D00602 -t 2>&1)
expected="Going to send hpssadm_command = device unlock -drive -id 300,301,302,303,304,371,600,601,602"
echo $output
check "$output" "$expected"
echo

echo '----negativ'

echo './hac lock drive D0012300 -t'
output=$(./hac lock drive D0012300 -t 2>&1)
expected="Incorrect command: \"lock drive D0012300 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive D0012300 -t'
output=$(./hac unlock drive D0012300 -t 2>&1)
expected="Incorrect command: \"unlock drive D0012300 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac lock drive D0030000-D0030400,D0037100,D0060000-D0060200 -t'
output=$(./hac lock drive D0030000-D0030400,D0037100,D0060000-D0060200 -t 2>&1)
expected="Incorrect command: \"lock drive D0030000-D0030400,D0037100,D0060000-D0060200 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac unlock drive D0030000-D0030400,D0037100,D0060000-D0060200 -t'
output=$(./hac unlock drive D0030000-D0030400,D0037100,D0060000-D0060200 -t 2>&1)
expected="Incorrect command: \"unlock drive D0030000-D0030400,D0037100,D0060000-D0060200 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---device repair command'
echo '----positive'

echo './hac rep dev 12 -t'
output=$(./hac rep dev 12 -t 2>&1)
expected="Going to send hpssadm_command = device repair -device -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac rep dev 123 -t'
output=$(./hac rep dev 123 -t 2>&1)
expected="Going to send hpssadm_command = device repair -device -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac rep reset dev 12 -t'
output=$(./hac rep reset dev 12 -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 12 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device repair -device -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac rep reset dev 123 -t'
output=$(./hac rep reset dev 123 -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 123 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 123 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 123 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device repair -device -id 123"
echo $output
check "$output" "$expected"
echo

echo '---drive repair command'
echo '----positive'

echo './hac rep drive 12 -t'
output=$(./hac rep drive 12 -t 2>&1)
expected="Going to send hpssadm_command = device repair -drive -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac rep drive 123 -t'
output=$(./hac rep drive 123 -t 2>&1)
expected="Going to send hpssadm_command = device repair -drive -id 123"
echo $output
check "$output" "$expected"
echo


echo 'drive dismount command'
echo '----positive'

echo './hac dism drive 12 -t'
output=$(./hac dism drive 12 -t 2>&1)
expected="Going to send hpssadm_command = device dismount -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac dism drive 123 -t'
output=$(./hac dism drive 123 -t 2>&1)
expected="Going to send hpssadm_command = device dismount -id 123"
echo $output
check "$output" "$expected"
echo

echo '---health status command'
echo '----positive'

echo './hac hs -t'
output=$(./hac hs -t 2>&1)
expected="Going to send hpssadm_command = health_status info"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac hs 12 -t'
output=$(./hac hs 12 -t 2>&1)
expected="Incorrect command: \"hs 12 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac hs lala -t'
output=$(./hac hs lala -t 2>&1)
expected="Incorrect command: \"hs lala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---help commands'
echo '----positive'

echo './hac helpha -t'
output=$(./hac helpha -t 2>&1)
expected="Going to send hpssadm_command = help"
echo $output
check "$output" "$expected"
echo


echo '--list commands'
echo '---list type commands'
echo '----positive'

echo './hac list alev -t'
output=$(./hac list alev -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Alarms and Events\""
echo $output
check "$output" "$expected"
echo


echo './hac list cos -t'
output=$(./hac list cos -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Classes of Service\""
echo $output
check "$output" "$expected"
echo


echo './hac list devdr -t'
output=$(./hac list devdr -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Devices and Drives\""
echo $output
check "$output" "$expected"
echo


echo './hac list filefam -t'
output=$(./hac list filefam -t 2>&1)
expected="Going to send hpssadm_command = list -type \"File Families\""
echo $output
check "$output" "$expected"
echo


echo './hac list fsjl -t'
output=$(./hac list fsjl -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Filesets & Junctions List\""
echo $output
check "$output" "$expected"
echo


echo './hac list hi -'
output=$(./hac list hi -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Hierarchies\""
echo $output
check "$output" "$expected"
echo


echo './hac list logpol -t'
output=$(./hac list logpol -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Logging Policies\""
echo $output
check "$output" "$expected"
echo


echo './hac list migpol -t'
output=$(./hac list migpol -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Migration Policies\""
echo $output
check "$output" "$expected"
echo


echo './hac list confsc -'
output=$(./hac list confsc -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Configured Storage Classes\""
echo $output
check "$output" "$expected"
echo


echo './hac list activesc -t'
output=$(./hac list activesc -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Active Storage Classes\""
echo $output
check "$output" "$expected"
echo


echo './hac list purpol -t'
output=$(./hac list purpol -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Purge Policies\""
echo $output
check "$output" "$expected"
echo


echo './hac list restus -t'
output=$(./hac list restus -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Restricted Users\""
echo $output
check "$output" "$expected"
echo


echo './hac list rtmsum -t'
output=$(./hac list rtmsum -t 2>&1)
expected="Going to send hpssadm_command = list -type \"RTM Summary List\""
echo $output
check "$output" "$expected"
echo


echo './hac list srv -t'
output=$(./hac list srv -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Servers\""
echo $output
check "$output" "$expected"
echo


echo './hac list subsys -t'
output=$(./hac list subsys -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Subsystems\""
echo $output
check "$output" "$expected"
echo


echo './hac list tmreq -t'
output=$(./hac list tmreq -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Tape Mount Requests\""
echo $output
check "$output" "$expected"
echo


echo './hac list tcreq -t'
output=$(./hac list tcreq -t 2>&1)
expected="Going to send hpssadm_command = list -type \"Tape Check-In Requests\""
echo $output
check "$output" "$expected"
echo


echo '--pvl job commands'
echo '---pvl job list commands'
echo '----positive'

echo './hac list pvlall -t'
output=$(./hac list pvlall -t 2>&1)
expected="Going to send hpssadm_command = pvl_job list"
echo $output
check "$output" "$expected"
echo


echo './hac list pvlniu -t'
output=$(./hac list pvlniu -t 2>&1)
expected="Going to send hpssadm_command = pvl_job list -not_in_use"
echo $output
check "$output" "$expected"
echo


echo './hac list pvlid 12345678 -t'
output=$(./hac list pvlid 12345678 -t 2>&1)
expected="Going to send hpssadm_command = pvl_job list -id 12345678"
echo $output
check "$output" "$expected"
echo


echo '---pvl job info command'
echo '----positive'

echo './hac info pvlid 12345678 -t'
output=$(./hac info pvlid 12345678 -t 2>&1)
expected="Going to send hpssadm_command = pvl_job info -id 12345678"
echo $output
check "$output" "$expected"
echo


echo './hac info pvlid 1234567890 -t'
echo '----negative'
output=$(./hac info pvlid 1234567890 -t 2>&1)
expected="Incorrect command: \"info pvlid 1234567890 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---pvl job cancel command'
echo '----positive'

echo './hac cancel pvlid 12345678 -t'
output=$(./hac cancel pvlid 12345678 -t 2>&1)
expected="Going to send hpssadm_command = pvl_job cancel -id 12345678"
echo $output
check "$output" "$expected"
echo


echo '---rtm commands'
echo '----positive'

echo './hac list rtmall -t'
output=$(./hac list rtmall -t 2>&1)
expected="Going to send hpssadm_command = rtm list"
echo $output
check "$output" "$expected"
echo


echo '---volume create disk commands'
echo '----positive'

echo './hac create disk scid 10 vol D00300 -t'
output=$(./hac create disk scid 10 vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac create disk scid 10 vol D00300 D00400 D00500 -t'
output=$(./hac create disk scid 10 vol D00300 D00400 D00500 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00400
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac create disk scid 10 vol D00300-D00302,D00500 -t'
output=$(./hac create disk scid 10 vol D00300-D00302,D00500 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00301
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00302
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac create disk scid 10 vol D0030000-D0030200,D0050000 -t'
output=$(./hac create disk scid 10 vol D0030000-D0030200,D0050000 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00301
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00302
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac create disk scid 10 vol D0030000-D00302,D0050000 -t'
output=$(./hac create disk scid 10 vol D0030000-D00302,D0050000 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00301
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00302
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo "(./hac create disk scname '10 | Storage Class 10' vol D00300 -t"
output=$(./hac create disk scname '10 | Storage Class 10' vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac create disk down scid 10 vol D0030000 -t'
output=$(./hac create disk down scid 10 vol D0030000 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo "./hac create disk down scname '10 | Storage Class 10' vol D00300 -t"
output=$(./hac create disk down scname '10 | Storage Class 10' vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo

touch disks-test.txt
echo "D00300" > disks-test.txt
echo "D00400" >> disks-test.txt


echo './hac create disk scid 10 file disks-test.txt -t'
output=$(./hac create disk scid 10 file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00400"
echo $output
check "$output" "$expected"
echo


echo "./hac create disk scname '10 | Storage Class 10' file disks-test.txt -t"
output=$(./hac create disk scname '10 | Storage Class 10' file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Volume Label\" D00400"
echo $output
check "$output" "$expected"
echo


echo './hac create disk down scid 10 file disks-test.txt -t'
output=$(./hac create disk down scid 10 file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Volume Label\" D00400"
echo $output
check "$output" "$expected"
echo


echo "./hac create disk down scname '10 | Storage Class 10' file disks-test.txt -t"
output=$(./hac create disk down scname '10 | Storage Class 10' file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Volume Label\" D00300
Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Volume Label\" D00400"
echo $output
check "$output" "$expected"
echo


echo './hac create disk scid 10 vol D00300 count 2 inc 1 -t'
output=$(./hac create disk scid 10 vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo "./hac create disk scname '10 | Storage Class 10' vol D00300 count 2 inc 1 -t"
output=$(./hac create disk scname '10 | Storage Class 10' vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac create disk down scid 10 vol D00300 count 2 inc 1 -t'
output=$(./hac create disk down scid 10 vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" 10 -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo "./hac create disk down scname '10 | Storage Class 10' vol D00300 count 2 inc 1 -t"
output=$(./hac create disk down scname '10 | Storage Class 10' vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume create_disk -field \"Initialization Fields\" -subfield \"Storage Class\" \"10 | Storage Class 10\" -field \"Optional\" -subfield \"PV Size\" 7811956728KB -field \"Optional\" -subfield \"Create DOWN\" \"on\" -field \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo '---volume create tape commands'
echo '----positive'

echo './hac create tape scid 123 vol 123456 -t'
output=$(./hac create tape scid 123 vol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo './hac create tape scid 123 vol A11111 -t'
output=$(./hac create tape scid 123 vol A11111 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use starting label\" -subfield \"Volume Label\" A11111"
echo $output
check "$output" "$expected"
echo


echo './hac create tape scid 123 vol A11111 A11112 A11113 -t'
output=$(./hac create tape scid 123 vol A11111 A11112 A11113 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use starting label\" -subfield \"Volume Label\" A11111 -field \"Use starting label\" -subfield \"Volume Label\" A11112 -field \"Use starting label\" -subfield \"Volume Label\" A11113"
echo $output
check "$output" "$expected"
echo


echo './hac create tape scid 123 vol A11111,A11112,A11113 -t'
output=$(./hac create tape scid 123 vol A11111,A11112,A11113 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use starting label\" -subfield \"Volume Label\" A11111 -field \"Use starting label\" -subfield \"Volume Label\" A11112 -field \"Use starting label\" -subfield \"Volume Label\" A11113"
echo $output
check "$output" "$expected"
echo


echo './hac create tape scid 123 vol A11111-A11113,A11115 -t'
output=$(./hac create tape scid 123 vol A11111-A11113,A11115 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use starting label\" -subfield \"Volume Label\" A11111 -field \"Use starting label\" -subfield \"Volume Label\" A11112 -field \"Use starting label\" -subfield \"Volume Label\" A11113 -field \"Use starting label\" -subfield \"Volume Label\" A11115"
echo $output
check "$output" "$expected"
echo


echo "./hac create tape scname '123 | Storage Class 123' vol A11111 -t"
output=$(./hac create tape scname '123 | Storage Class 123' vol A11111 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" \"123 | Storage Class 123\" -field \"Use starting label\" -subfield \"Volume Label\" A11111"
echo $output
check "$output" "$expected"
echo

touch tapes-test.txt
echo "A11111" > tapes-test.txt
echo "A11112" >> tapes-test.txt
curdir=$(pwd)

echo './hac create tape scid 123 file tapes-test.txt -t'
output=$(./hac create tape scid 123 file tapes-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use list in external file\" -subfield \"File\" \"$curdir/tapes-test.txt\""
echo $output
check "$output" "$expected"
echo


echo "./hac create tape scname '123 | Storage Class 123' file tapes-test.txt -t"
output=$(./hac create tape scname '123 | Storage Class 123' file tapes-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" \"123 | Storage Class 123\" -field \"Use list in external file\" -subfield \"File\" \"$curdir/tapes-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac create tape scid 123 vol 123456 count 10 inc 1 -t'
output=$(./hac create tape scid 123 vol 123456 count 10 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" 123 -field \"Use starting label\" -subfield \"Fill Count\" 10 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo "./hac create tape scname '123 | Storage Class 123' vol 123456 count 10 inc 1 -t"
output=$(./hac create tape scname '123 | Storage Class 123' vol 123456 count 10 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume create_tape -field \"Initialization Fields\" -subfield \"Storage Class\" \"123 | Storage Class 123\" -field \"Use starting label\" -subfield \"Fill Count\" 10 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo

echo '---volume import disk commands'
echo '----positive'

echo './hac import def disk it def vol D00300 -t'
output=$(./hac import def disk it def vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - Default Disk\" -field \"Import Type\" \"Default Import\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac import def disk it def vol D00300 D00400 D00500 -t'
output=$(./hac import def disk it def vol D00300 D00400 D00500 -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - Default Disk\" -field \"Import Type\" \"Default Import\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00400 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac import def disk it def vol D00300-D00302 -t'
output=$(./hac import def disk it def vol D00300-D00302 -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - Default Disk\" -field \"Import Type\" \"Default Import\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00301 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00302"
echo $output
check "$output" "$expected"
echo


echo './hac import def disk it sc file disks-test.txt -t'
output=$(./hac import def disk it sc file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - Default Disk\" -field \"Import Type\" \"Scratch Import\" -field \"Build List of Volumes\" -subfield \"Use list in external file\" -subfield \"File\" \"$curdir/disks-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac import def disk it ow vol D00300 count 2 inc 1 -t'
output=$(./hac import def disk it ow vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - Default Disk\" -field \"Import Type\" \"Overwrite Import\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac import san disk it def vol D00300 -t'
output=$(./hac import san disk it def vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - SAN Attached Disk\" -field \"Import Type\" \"Default Import\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac import san disk it def file disks-test.txt -t'
output=$(./hac import san disk it def file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - SAN Attached Disk\" -field \"Import Type\" \"Default Import\" -field \"Build List of Volumes\" -subfield \"Use list in external file\" -subfield \"File\" \"$curdir/disks-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac import san disk it def vol D00300 count 2 inc 1 -t'
output=$(./hac import san disk it def vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume import_disk -field \"Media Type\" \"Generic - SAN Attached Disk\" -field \"Import Type\" \"Default Import\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo

echo '---volume import tape commands'
echo '----positive'

echo './hac import tape pvr lib1 ndr 1 mt lto8 it def vol 123456 -t'
output=$(./hac import tape pvr lib1 ndr 1 mt lto8 it def vol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume import_tape -field \"PVR\" \"LIB1_NAME\" -field \"Max Number of Drives\" 1 -field \"Import Information\" -subfield \"Media Type\" \"IBM - 3580 (LTO) Gen8 Tape\" -field \"Import Information\" -subfield \"Import Type\" \"Default Import\" -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo './hac import tape pvr lib1 ndr 1 mt lto8 it def vol 111111 111112 111113 -t'
output=$(./hac import tape pvr lib1 ndr 1 mt lto8 it def vol 111111 111112 111113 -t 2>&1)
expected="Going to send hpssadm_command = volume import_tape -field \"PVR\" \"LIB1_NAME\" -field \"Max Number of Drives\" 1 -field \"Import Information\" -subfield \"Media Type\" \"IBM - 3580 (LTO) Gen8 Tape\" -field \"Import Information\" -subfield \"Import Type\" \"Default Import\" -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111111 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111112 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111113"
echo $output
check "$output" "$expected"
echo


echo './hac import tape pvr lib1 ndr 1 mt lto8 it def vol 111111,111112,111113 -t'
output=$(./hac import tape pvr lib1 ndr 1 mt lto8 it def vol 111111,111112,111113 -t 2>&1)
expected="Going to send hpssadm_command = volume import_tape -field \"PVR\" \"LIB1_NAME\" -field \"Max Number of Drives\" 1 -field \"Import Information\" -subfield \"Media Type\" \"IBM - 3580 (LTO) Gen8 Tape\" -field \"Import Information\" -subfield \"Import Type\" \"Default Import\" -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111111 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111112 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111113"
echo $output
check "$output" "$expected"
echo


echo './hac import tape pvr lib1 ndr 1 mt lto8 it def vol 111111-111113 -t'
output=$(./hac import tape pvr lib1 ndr 1 mt lto8 it def vol 111111-111113 -t 2>&1)
expected="Going to send hpssadm_command = volume import_tape -field \"PVR\" \"LIB1_NAME\" -field \"Max Number of Drives\" 1 -field \"Import Information\" -subfield \"Media Type\" \"IBM - 3580 (LTO) Gen8 Tape\" -field \"Import Information\" -subfield \"Import Type\" \"Default Import\" -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111111 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111112 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 111113"
echo $output
check "$output" "$expected"
echo


echo './hac import tape pvr lib1 ndr 1 mt lto8 it def file tapes-test.txt -t'
output=$(./hac import tape pvr lib1 ndr 1 mt lto8 it def file tapes-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume import_tape -field \"PVR\" \"LIB1_NAME\" -field \"Max Number of Drives\" 1 -field \"Import Information\" -subfield \"Media Type\" \"IBM - 3580 (LTO) Gen8 Tape\" -field \"Import Information\" -subfield \"Import Type\" \"Default Import\" -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use list in external file\" -subfield \"File\" \"$curdir/tapes-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac import tape pvr lib1 ndr 1 mt lto8 it def vol 123456 count 2 inc 1'
output=$(./hac import tape pvr lib1 ndr 1 mt lto8 it def vol 123456 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume import_tape -field \"PVR\" \"LIB1_NAME\" -field \"Max Number of Drives\" 1 -field \"Import Information\" -subfield \"Media Type\" \"IBM - 3580 (LTO) Gen8 Tape\" -field \"Import Information\" -subfield \"Import Type\" \"Default Import\" -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Import Information\" -subfield \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo

echo '---volume delete disk resources commands'
echo '----positive'

echo './hac del disk vol D00300 -t'
output=$(./hac del disk vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac del disk vol D00300 D00400 D00500 -t'
output=$(./hac del disk vol D00300 D00400 D00500 -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 400 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 400 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 400 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Use starting label\" -subfield \"Volume Label\" D00400 -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac del disk vol D00300,D00400,D00500 -t'
output=$(./hac del disk vol D00300,D00400,D00500 -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 400 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 400 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 400 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Use starting label\" -subfield \"Volume Label\" D00400 -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac del disk vol D00300-D00302,D00500 -t'
output=$(./hac del disk vol D00300-D00302,D00500 -t 2>&1)
expected="Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 300 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 301 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 301 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 301 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 302 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 302 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 302 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Bytes Read\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Bytes Written\"
Going to send hpssadm_command = device update -type \"Mover Device Information\" -id 500 -field \"Device Record\" -subfield \"Number Of Errors\"
Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Use starting label\" -subfield \"Volume Label\" D00301 -field \"Use starting label\" -subfield \"Volume Label\" D00302 -field \"Use starting label\" -subfield \"Volume Label\" D00500"
echo $output
check "$output" "$expected"
echo


echo './hac del disk file disks-test.txt -t'
output=$(./hac del disk file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use list in external file\" -subfield \"File\" \"$curdir/disks-test.txt\""
echo $output
check "$output" "$expected"
echo


echo '---volume delete tape resources commands'
echo '----positive'

echo './hac del tape vol 123456 -t'
output=$(./hac del tape vol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo './hac del tape vol 123456 123457 123458 -t'
output=$(./hac del tape vol 123456 123457 123458 -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" 123456 -field \"Use starting label\" -subfield \"Volume Label\" 123457 -field \"Use starting label\" -subfield \"Volume Label\" 123458"
echo $output
check "$output" "$expected"
echo


echo './hac del tape vol 123456,123457,123458 -t'
output=$(./hac del tape vol 123456,123457,123458 -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" 123456 -field \"Use starting label\" -subfield \"Volume Label\" 123457 -field \"Use starting label\" -subfield \"Volume Label\" 123458"
echo $output
check "$output" "$expected"
echo


echo './hac del tape vol 123456-123458,123460 -t'
output=$(./hac del tape vol 123456-123458,123460 -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Volume Label\" 123456 -field \"Use starting label\" -subfield \"Volume Label\" 123457 -field \"Use starting label\" -subfield \"Volume Label\" 123458 -field \"Use starting label\" -subfield \"Volume Label\" 123460"
echo $output
check "$output" "$expected"
echo


echo './hac del tape file tapes-test.txt -t'
output=$(./hac del tape file tapes-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use list in external file\" -subfield \"File\" \"$curdir/tapes-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac del tape vol 123456 count 10 inc 1 -t'
output=$(./hac del tape vol 123456 count 10 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume delete -field \"Use starting label\" -subfield \"Fill Count\" 10 -field \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo

echo '---volume export disk commands'
echo '----positive'

echo './hac export disk vol D00300 -t'
output=$(./hac export disk vol D00300 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo './hac export disk vol D00300 D00301 D00302 -t'
output=$(./hac export disk vol D00300 D00301 D00302 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00301 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00302"
echo $output
check "$output" "$expected"
echo


echo './hac export disk vol D00300,D00301,D00302 -t'
output=$(./hac export disk vol D00300,D00301,D00302 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00301 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00302"
echo $output
check "$output" "$expected"
echo


echo './hac export disk vol D00300-D00302 -t'
output=$(./hac export disk vol D00300-D00302 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00301 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00302"
echo $output
check "$output" "$expected"
echo


echo './hac export disk file disks-test.txt -t'
output=$(./hac export disk file disks-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use list in external file\" -subfield \"File\" \"$curdir/disks-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac export disk vol D00300 count 2 inc 1 -t'
output=$(./hac export disk vol D00300 count 2 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Count\" 2 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" D00300"
echo $output
check "$output" "$expected"
echo


echo '---volume export tape commands'
echo '----positive'

echo './hac export tape vol 123456 -t'
output=$(./hac export tape vol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo './hac export tape vol 123456 123457 123458 -t'
output=$(./hac export tape vol 123456 123457 123458 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123457 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123458"
echo $output
check "$output" "$expected"
echo


echo './hac export tape vol 123456,123457,123458 -t'
output=$(./hac export tape vol 123456,123457,123458 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123457 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123458"
echo $output
check "$output" "$expected"
echo


echo './hac export tape vol 123456-123458 -t'
output=$(./hac export tape vol 123456-123458 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123457 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123458"
echo $output
check "$output" "$expected"
echo


echo './hac export tape eject vol 123456 -t'
output=$(./hac export tape eject vol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume export -filed \"Eject Tapes After Exporting\" \"yes\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo './hac export tape file tapes-test.txt -t'
output=$(./hac export tape file tapes-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use list in external file\" -subfield \"File\" \"$curdir/tapes-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac export tape eject file tapes-test.txt -t'
output=$(./hac export tape eject file tapes-test.txt -t 2>&1)
expected="Going to send hpssadm_command = volume export -filed \"Eject Tapes After Exporting\" \"yes\" -field \"Build List of Volumes\" -subfield \"Use list in external file\" -subfield \"File\" \"$curdir/tapes-test.txt\""
echo $output
check "$output" "$expected"
echo


echo './hac export tape vol 123456 count 10 inc 1 -t'
output=$(./hac export tape vol 123456 count 10 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume export -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Count\" 10 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo './hac export tape eject vol 123456 count 10 inc 1 -t'
output=$(./hac export tape eject vol 123456 count 10 inc 1 -t 2>&1)
expected="Going to send hpssadm_command = volume export -filed \"Eject Tapes After Exporting\" \"yes\" -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Count\" 10 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Fill Increment\" 1 -field \"Build List of Volumes\" -subfield \"Use starting label\" -subfield \"Volume Label\" 123456"
echo $output
check "$output" "$expected"
echo


echo '---task list command'
echo '----positive'

echo './hac task list -t'
output=$(./hac task list -t 2>&1)
expected="Going to send hpssadm_command = task list"
echo $output
check "$output" "$expected"
echo


echo '---volume info commands'
echo '----positive'

echo './hac info pvlvol 123456 -t'
output=$(./hac info pvlvol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume info -type \"PVL Volume Information\" -name 123456"
echo $output
check "$output" "$expected"
echo


echo './hac info pvrvol 123456 -t'
output=$(./hac info pvrvol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume info -type \"PVR Cartridge Information\" -name 123456"
echo $output
check "$output" "$expected"
echo


echo './hac info csvol 123456 -t'
output=$(./hac info csvol 123456 -t 2>&1)
expected="Going to send hpssadm_command = volume info -type \"Core Server Tape Volume\" -name 123456"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac info pvlvol -t'
output=$(./hac info pvlvol -t 2>&1)
expected="Incorrect command: \"info pvlvol -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info pvlvol 1 -t'
output=$(./hac info pvlvol 1 -t 2>&1)
expected="Incorrect command: \"info pvlvol 1 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info pvlvol 12345678 -t'
output=$(./hac info pvlvol 12345678 -t 2>&1)
expected="Incorrect command: \"info pvlvol 12345678 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '--volume update commands'
echo '---pvr cartridge'
echo '----positive'

echo './hac reset pvrvol 123456 mnt -t'
output=$(./hac reset pvrvol 123456 mnt -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"PVR Cartridge Information\" -name 123456 -field \"Mounts Since Maintenance\""
echo $output
check "$output" "$expected"
echo


echo '----negative'

echo './hac reset pvrvol 12345678 mnt -t'
output=$(./hac reset pvrvol 12345678 mnt -t 2>&1)
expected="Incorrect command: \"reset pvrvol 12345678 mnt -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac reset pvrvol mnt -t'
output=$(./hac reset pvrvol mnt -t 2>&1)
expected="Incorrect command: \"reset pvrvol mnt -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac reset pvrvol 123456 lal -t'
output=$(./hac reset pvrvol 123456 lal -t 2>&1)
expected="Incorrect command: \"reset pvrvol 123456 lal -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac reset pvlvol 123456 mnt -t'
output=$(./hac reset pvlvol 123456 mnt -t 2>&1)
expected="Incorrect command: \"reset pvlvol 123456 mnt -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac reset csvol 123456 mnt -t'
output=$(./hac reset csvol 123456 mnt -t 2>&1)
expected="Incorrect command: \"reset csvol 123456 mnt -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---pvl volume'
echo '----positive'

echo './hac update pvlvol 123456 comment lalala -t'
output=$(./hac update pvlvol 123456 comment lalala -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"PVL Volume Information\" -name 123456 -field \"Comment\" \"lalala\""
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 123456 comment set 123456 down date: 10-10-2010 reason: reason previous_state: EOM -t'
output=$(./hac update pvlvol 123456 comment set 123456 down date: 10-10-2010 reason: reason previous_state: EOM -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"PVL Volume Information\" -name 123456 -field \"Comment\" \"set 123456 down date: 10-10-2010 reason: reason previous_state: EOM\""
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 123456 rm comment -'
output=$(./hac update pvlvol 123456 rm comment -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"PVL Volume Information\" -name 123456 -field \"Comment\" \"\""
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac update pvlvol 123456 comment -t'
output=$(./hac update pvlvol 123456 comment -t 2>&1)
expected="Incorrect command: \"update pvlvol 123456 comment -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 12345678 comment lalala -t'
output=$(./hac update pvlvol 12345678 comment lalala -t 2>&1)
expected="Incorrect command: \"update pvlvol 12345678 comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol comment lalala -t'
output=$(./hac update pvlvol comment lalala -t 2>&1)
expected="Incorrect command: \"update pvlvol comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 12 comment lalala -t'
output=$(./hac update pvlvol 12 comment lalala -t 2>&1)
expected="Incorrect command: \"update pvlvol 12 comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 12345678 comment lalala -t'
output=$(./hac update pvlvol 12345678 comment lalala -t 2>&1)
expected="Incorrect command: \"update pvlvol 12345678 comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 123456 -t'
output=$(./hac update pvlvol 123456 -t 2>&1)
expected="Incorrect command: \"update pvlvol 123456 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 123456 lala -t'
output=$(./hac update pvlvol 123456 lala -t 2>&1)
expected="Incorrect command: \"update pvlvol 123456 lala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvrvol 123456 comment lalala -t'
output=$(./hac update pvrvol 123456 comment lalala -t 2>&1)
expected="Incorrect command: \"update pvrvol 123456 comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update csvol 123456 comment lalala -t'
output=$(./hac update csvol 123456 comment lalala -t 2>&1)
expected="Incorrect command: \"update csvol 123456 comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 123456 rm comment lalala -t'
output=$(./hac update pvlvol 123456 rm comment lalala -t 2>&1)
expected="Incorrect command: \"update pvlvol 123456 rm comment lalala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---cs volume'
echo '----positive'

echo './hac update csvol 123456 down -t'
output=$(./hac update csvol 123456 down -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Tape Volume\" -name 123456 -field \"VV Condition\" DOWN"
echo $output
check "$output" "$expected"
echo


echo './hac update csvol 123456 rwc -t'
output=$(./hac update csvol 123456 rwc -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Tape Volume\" -name 123456 -field \"VV Condition\" RWC"
echo $output
check "$output" "$expected"
echo


echo './hac update csvol 123456 ro -t'
output=$(./hac update csvol 123456 ro -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Tape Volume\" -name 123456 -field \"VV Condition\" RO"
echo $output
check "$output" "$expected"
echo


echo './hac update csvol 123456 eom -t'
output=$(./hac update csvol 123456 eom -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Tape Volume\" -name 123456 -field \"VV Condition\" EOM"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac update csvol 123456 lala -t'
output=$(./hac update csvol 123456 lala -t 2>&1)
expected="Incorrect command: \"update csvol 123456 lala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update csvol 123456 -t'
output=$(./hac update csvol 123456 -t 2>&1)
expected="Incorrect command: \"update csvol 123456 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac update pvlvol 123456 down -t'
output=$(./hac update pvlvol 123456 down -t 2>&1)
expected="Incorrect command: \"update pvlvol 123456 down -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---cs volume'
echo '----positive'

echo './hac update csdisk 123456 down -t'
output=$(./hac update csdisk 123456 down -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Disk Volume\" -name 123456 -field \"VV Condition\" DOWN"
echo $output
check "$output" "$expected"
echo


echo './hac update csdisk 123456 rwc -t'
output=$(./hac update csdisk 123456 rwc -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Disk Volume\" -name 123456 -field \"VV Condition\" RWC"
echo $output
check "$output" "$expected"
echo


echo './hac update csdisk 123456 ro -t'
output=$(./hac update csdisk 123456 ro -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Disk Volume\" -name 123456 -field \"VV Condition\" RO"
echo $output
check "$output" "$expected"
echo


echo './hac update csdisk 123456 eom -t'
output=$(./hac update csdisk 123456 eom -t 2>&1)
expected="Going to send hpssadm_command = volume update -type \"Core Server Disk Volume\" -name 123456 -field \"VV Condition\" EOM"
echo $output
check "$output" "$expected"
echo


echo '---storage class list commands'
echo '----positive'

echo './hac list scall -t'
output=$(./hac list scall -t 2>&1)
expected="Going to send hpssadm_command = storage_class list -all"
echo $output
check "$output" "$expected"
echo


echo './hac list scsick -t'
output=$(./hac list scsick -t 2>&1)
expected="Going to send hpssadm_command = storage_class list -sick"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac list sclala -t'
output=$(./hac list sclala -t 2>&1)
expected="Incorrect command: \"list sclala -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---storage class info command'
echo '----positive'

echo './hac info scid 12 -t'
output=$(./hac info scid 12 -t 2>&1)
expected="Going to send hpssadm_command = storage_class info -id 12"
echo $output
check "$output" "$expected"
echo


echo './hac info scid 123 -t'
output=$(./hac info scid 123 -t 2>&1)
expected="Going to send hpssadm_command = storage_class info -id 123"
echo $output
check "$output" "$expected"
echo


echo './hac info scid 1234 -t'
output=$(./hac info scid 1234 -t 2>&1)
expected="Going to send hpssadm_command = storage_class info -id 1234"
echo $output
check "$output" "$expected"
echo

echo '----negative'

echo './hac info -t'
output=$(./hac info -t 2>&1)
expected="Incorrect command: \"info -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info scid 12345 -t'
output=$(./hac info scid 12345 -t 2>&1)
expected="Incorrect command: \"info scid 12345 -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo './hac info scid -t'
output=$(./hac info scid -t 2>&1)
expected="Incorrect command: \"info scid -t\". Refer to 'hac help' for usage."
echo $output
check "$output" "$expected"
echo


echo '---storage class migration controls'
echo '----positive'

echo './hac migr start scid 10 -t'
output=$(./hac migr start scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class migrate -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac migr stop scid 10 -t'
output=$(./hac migr stop scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class migrate_stop -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac migr resume scid 10 -t'
output=$(./hac migr resume scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class migrate_resume -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac migr suspend scid 10 -t'
output=$(./hac migr suspend scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class migrate_suspend -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac migr reread scid 10 -t'
output=$(./hac migr reread scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class migrate_reread_policy -id 10"
echo $output
check "$output" "$expected"
echo


echo '---storage class purge controls'
echo '----positive'

echo './hac purge start scid 10 -t'
output=$(./hac purge start scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class purge -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac purge stop scid 10 -t'
output=$(./hac purge stop scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class purge_stop -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac purge resume scid 10 -t'
output=$(./hac purge resume scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class purge_resume -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac purge suspend scid 10 -t'
output=$(./hac purge suspend scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class purge_suspend -id 10"
echo $output
check "$output" "$expected"
echo


echo './hac purge reread scid 10 -t'
output=$(./hac purge reread scid 10 -t 2>&1)
expected="Going to send hpssadm_command = storage_class purge_reread_policy -id 10"
echo $output
check "$output" "$expected"
echo

rm -f disks-test.txt tapes-test.txt

if [[ $passed == 0 ]]; then
    echo "----One or more tests failed."
    exit 1
else
    echo "----All tests passed."
    exit 0
fi 

